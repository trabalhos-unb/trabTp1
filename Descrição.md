## Relacionamentos das Entidades
 	Gerente de Projeto
	  * Pode gerenciar até 5 projetos
	Gerente de Sistema
	  * Existe somente um no sistema inteiro
	Desenvolvedor
	  * Pode participar de 0 a 10 projetos
	Projeto
	  * É gerenciado por somente um gerente de projeto
	  * Podem ter de 0 a 10 desenvolvedores

## Atributos das Entidades
	Gerente de Sistema
	  *nome
	  *matricula
	  *senha
	Gerente de Projeto
	  *nome
	  *matricula
	  *senha
	  *telefone
	Desenvolvedor
	  *nome
	  *matricula
	  *senha
	  *email
	  *função(cargo)
	Projeto
	  *nome
	  *codigo
	  *gerente de projeto( gerente responsável pelo projeto )
	  *desenvolvedores do projetos ( desenvolvedores alocados para projeto )
	  *estado atual
	  *fase iniciacao 'fase atual de desenvolvimento
	  *fase preparacao
	  *fase execucao
	  *fase encerrament

## Regras de Negócio
	Projeto
	 *Custo atual não pode ser superior ao previsto

	Gerente de Sistema
	  *Cadastra gerente de projeto
	  *Descadastra gerente de projeto, somente se, todos os projeto que o
		respectivo gerente de projeto gerencia, estiverem inativos
	Gerente de Projeto
	  *Pode alterar informações pessoais exceto matricula
	  *Pode alterar informações do projeto que gerencia, exceto o codigo
	  *Não pode alterar informações acerca de projeto encerrado

	  *Cadastra projetos
	  *Descadastra projetos existentes
	  *Cadastra desenvolvedores
	  *Descadastra desenvolvedores, somente se nao estiver associado
		a nenhum projeto ou a um projeto inativo
	  *Associa(aloca) desenvolvedores aos projetos que gerencia
	
	Desenvolvedor
	  *Pode alterar informações pessoais exceto matricula

- Conveção camelCase
funtion(){

}

