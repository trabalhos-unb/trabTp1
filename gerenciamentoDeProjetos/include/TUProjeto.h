#ifndef TUPROJETO_H
#define TUPROJETO_H

#include "Projeto.h"

class TUProjeto{
    private:
        Projeto *projeto;
        void setUp();
        void tearDown();
        void cenarioSucesso();
        void cenarioFalha();
        int estado;
    public:
        const static int SUCESSO;
        const static int FALHA;

        int run();
};

#endif // TUPROJETO_H
