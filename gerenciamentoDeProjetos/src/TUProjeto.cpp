#include "TUProjeto.h"

const int TUProjeto::SUCESSO = 1;
const int TUProjeto::FALHA = 0;

void TUProjeto::setUp(){
    estado = SUCESSO;
}

//void TUProjeto::tearDown(){
//    delete projeto;
//}

void TUProjeto::cenarioSucesso(){
    try{
    projeto = new Projeto(new Nome("MARIA"), new CodigoProjeto("ABCDE"), new GerenteDeProjeto(), new Data("02/08/2016"), new Data("10/10/2017"), new Custo(1.0), new Custo(42.0), new EstadoProjeto(1));
    Desenvolvedor *cris = new Desenvolvedor(new Nome("CRIS"), new Matricula("12345"), new Senha("12345"), new Email("cristiane@gmail.com"), new Funcao(3));
    projeto->addDesenvolvedor(cris);
    }
    catch(invalid_argument excecao){
        estado = FALHA;
        cout << excecao.what() << endl;
    }

}
void TUProjeto::cenarioFalha(){
    try{
        projeto = new Projeto(new Nome("Maria"), new CodigoProjeto("ABCDE"), new GerenteDeProjeto(), new Data("02/08/2016"), new Data("10/10/2017"), new Custo(1.0), new Custo(42.0), new EstadoProjeto(1));
        Desenvolvedor *cris = new Desenvolvedor(new Nome("CRIS"), new Matricula("123456"), new Senha("123456"), new Email("cristiane@gmail.com"), new Funcao(3));
        projeto->addDesenvolvedor(cris);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}
int TUProjeto::run(){
    setUp();
    cenarioSucesso();
    cenarioFalha();
//    tearDown();
    return estado;
}
