var group___entidades =
[
    [ "Desenvolvedor", "class_desenvolvedor.html", [
      [ "Desenvolvedor", "class_desenvolvedor.html#a79500eb06aa1945e90667e0e01fde033", null ],
      [ "Desenvolvedor", "class_desenvolvedor.html#a727d52a8783ddcef48828a40b5c0aaa0", null ],
      [ "~Desenvolvedor", "class_desenvolvedor.html#a71630be9b8027e5c8e37302ae9bf9fd8", null ],
      [ "getNome", "class_desenvolvedor.html#ac1020584581e4082ad795abfc8730108", null ]
    ] ],
    [ "FaseEntidade", "class_fase_entidade.html", [
      [ "FaseEntidade", "class_fase_entidade.html#a585b00e5933dba8ad82b9f5aa49d4b6a", null ],
      [ "FaseEntidade", "class_fase_entidade.html#aed23d6e6cd83eb834a4dfbea26b13455", null ],
      [ "~FaseEntidade", "class_fase_entidade.html#a3ce2e0c852a4890fe650c80e2d95dfab", null ],
      [ "getDataInicio", "class_fase_entidade.html#ac61b06afe46146943924480e84beaf51", null ],
      [ "getDataTermino", "class_fase_entidade.html#a4f64fee67598845973a8077bdbd35b7a", null ],
      [ "getFaseProjeto", "class_fase_entidade.html#a29d2bdfcb0ed7756c90b9075251fcf86", null ]
    ] ],
    [ "GerenteDeProjeto", "class_gerente_de_projeto.html", [
      [ "GerenteDeProjeto", "class_gerente_de_projeto.html#a5b5c1d68f7e9d055e7769531c2d227cf", null ],
      [ "GerenteDeProjeto", "class_gerente_de_projeto.html#a82a6ba80795075c76378babf7ba9bb00", null ],
      [ "~GerenteDeProjeto", "class_gerente_de_projeto.html#ac0b94a3213d424e41971363fbe743b72", null ]
    ] ],
    [ "GerenteDeSistema", "class_gerente_de_sistema.html", [
      [ "GerenteDeSistema", "class_gerente_de_sistema.html#a2b6b1a83615fa32439ba7b2c3f33a102", null ],
      [ "GerenteDeSistema", "class_gerente_de_sistema.html#a8c83e84719ab9f9814729ebd92b79372", null ],
      [ "~GerenteDeSistema", "class_gerente_de_sistema.html#a321e6a9909535f1e466520fc444131ca", null ]
    ] ]
];