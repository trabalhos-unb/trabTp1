#ifndef FASE_ENTIDADE_H
#define FASE_ENTIDADE_H

#include "Dominios.h"

/**
 * \defgroup Entidades
 * @{
 */
/**@}*/


/**
\brief Classe que modela a entidade Fase
\ingroup Entidades
*/

class FaseEntidade{
    private:
        Data *dataInicio;
        Data *dataTermino;
        FaseProjeto *codigoFase;
    public:
        ///Construtor padrão, instancia os objetos com os valores padrao
        FaseEntidade();
        /**Construtor de Fase(Entidade)
        * Recebe como parametro valores validos dos tipos:
        * - Ponteiro para objeto Data(dataInicio)
        * - Ponteiro para objeto Data(dataTermino)
        * - Ponteiro para objeto FaseProjeto
        ** Nao possui validacao, dando que os objetos de dominio
        * possuem validacao em seu instanciamento.
        */
        FaseEntidade(Data *, Data *, FaseProjeto *);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~FaseEntidade();
        ///retorna a data de inicio
        string getDataInicio();
        ///retorna a data de termino
        string getDataTermino();
        ///retorna a fase do projeto
        int getFaseProjeto();


};

#endif // FASE_ENTIDADE_H
