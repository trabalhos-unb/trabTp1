#ifndef GERENTEDESISTEMA_ENTIDADE_H
#define GERENTEDESISTEMA_ENTIDADE_H

#include <iostream>
#include "../Dominios/Dominios.h"

/**
\brief Classe que modela a entidade Gerente de Sistema
\ingroup Entidades
*/

class GerenteDeSistema{
    private:
        Nome *nome;
        Matricula *matricula;
        Senha *senha;
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        GerenteDeSistema();
        /** Construtor de Gerente de Projeto
        * Recebe como parãmetros objetos válidos,
        * dos tipos:
        * - Ponteiro para objeto Nome
        * - Ponteiro para objeto Matricula
        * - Ponteiro para objeto Senha
        * Não possui validação, dado que os objetos de dominio
        * possuem validação em seu instanciamento.
        */
        GerenteDeSistema(Nome *, Matricula *, Senha *);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~GerenteDeSistema();
};

#endif // GERENTEDESISTEMA_ENTIDADE_H
