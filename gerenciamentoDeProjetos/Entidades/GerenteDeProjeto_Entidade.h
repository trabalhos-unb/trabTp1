#ifndef GERENTEDEPROJETO_ENTIDADE_H
#define GERENTEDEPROJETO_ENTIDADE_H

#include <iostream>
#include "../Dominios/Dominios.h"

/**
\brief Classe que modela a entidade Gerente de Projeto
\ingroup Entidades
*/

class GerenteDeProjeto{
    private:
        Nome *nome;
        Matricula *matricula;
        Senha *senha;
        Telefone *telefone;
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        GerenteDeProjeto();
        /** Construtor de Gerente de Projeto
        * Recebe como parãmetros objetos válidos,
        * dos tipos:
        * - Ponteiro para objeto Nome
        * - Ponteiro para objeto Matricula
        * - Ponteiro para objeto Senha
        * - Ponteiro para objeto Telefone
        ** Não possui validação, dado que os objetos de dominio
        * possuem validação em seu instanciamento.
        */
        GerenteDeProjeto(Nome *, Matricula *, Senha *, Telefone *);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~GerenteDeProjeto();
};

#endif // GERENTEDEPROJETO_ENTIDADE_H
