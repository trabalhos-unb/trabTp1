#ifndef DESENVOLVEDOR_ENTIDADE_H
#define DESENVOLVEDOR_ENTIDADE_H

#include <iostream>
#include "../Dominios/Dominios.h"
/**
 * \defgroup Entidades
 * @{
 */
/**@}*/


/**
\brief Classe que modela o dominio Desenvolvedor
\ingroup Entidades
*/

class Desenvolvedor{
    private:
        Nome *nome;
        Matricula *matricula;
        Senha *senha;
        Email *email;
        Funcao *funcao;
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        Desenvolvedor();
        string getNome();
        /** Construtor de Desenvolvedor
        * Recebe como parãmetros objetos válidos,
        * dos tipos:
        * - Ponteiro para objeto Nome
        * - Ponteiro para objeto Matricula
        * - Ponteiro para objeto Senha
        * - Ponteiro para objeto Email
        * - Ponteiro para objeto Funcao
        ** Não possui validação, dado que os objetos de dominio
        * possuem validação em seu instanciamento.
        */
        Desenvolvedor(Nome *, Matricula *, Senha *, Email *, Funcao *);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~Desenvolvedor();
};

#endif // DESENVOLVEDOR_ENTIDADE_H
