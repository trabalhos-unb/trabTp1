#ifndef PROJETO_H
#define PROJETO_H

#include "Dominios.h"
#include "Entidades.h"

#include <list>

/**
 * \defgroup Entidades
 * @{
 */
/**@}*/


/**
\brief Classe que modela a entidade Projeto
\ingroup Entidades
*/

using namespace std;

class Projeto{
    private:
        ///Lista contendo os desenvolvedores de um determinado projeto
        list<Desenvolvedor> desenvolvedores;
        Nome *nome;
        CodigoProjeto *codigo;
        GerenteDeProjeto *gerente;
        Data *dataInicio;
        Data *dataTermino;
        Custo *custoAtual;
        Custo *custoPrevisto;
        EstadoProjeto *estado;

    public:
        ///Construtor padrão, instancia os objetos com os valores padrao
        Projeto();
        /**Construtor de Projeto(Entidade)
        * Recebe como parametro valores validos dos tipos:
        * - Ponteiro para objeto Nome
        * - Ponteiro para objeto CodigoProjeto
        * - Ponteiro para objeto GerenteDeProjetoo
        * - Ponteiro para objeto Data(dataInicio)
        * - Ponteiro para objeto Data(dataTermino)
        * - Ponteiro para objeto Custo(inicial)
        * - Ponteiro para objeto Custo(previsto)
        * - Ponteiro para objeto EstadoProjeto
        ** Nao possui validacao, dando que os objetos de dominio
        * possuem validacao em seu instanciamento.
        */
        Projeto(Nome *, CodigoProjeto *, GerenteDeProjeto *, Data *, Data *, Custo*, Custo*, EstadoProjeto *);
        ~Projeto();
        ///Metodo para adicionar desenvolvedores para o projeto
        void addDesenvolvedor(Desenvolvedor *);
        ///Metodo para remover os desenvolvedores do projeto
        void removerDesenvolvedores();
        ///Metodo para saber a quantidade de desenvolvedores do projeto
        int quantDesenvolvedores();
};

#endif // PROJETO_H
