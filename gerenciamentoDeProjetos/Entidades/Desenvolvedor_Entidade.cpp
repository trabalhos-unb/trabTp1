#include "Desenvolvedor_Entidade.h"

Desenvolvedor::Desenvolvedor(){
    this->nome = new Nome();
    this->matricula = new Matricula();
    this->senha = new Senha();
    this->email = new Email();
    this->funcao = new Funcao();
};

Desenvolvedor::Desenvolvedor(Nome *nome, Matricula *matricula, Senha *senha, Email *email, Funcao *funcao){
    this->nome = nome;
    this->matricula = matricula;
    this->senha = senha;
    this->email = email;
    this->funcao = funcao;
};

Desenvolvedor::~Desenvolvedor(){
    delete nome;
    delete matricula;
    delete senha;
    delete email;
    delete funcao;
};
string Desenvolvedor::getNome(){
    return nome->getNome();
}
