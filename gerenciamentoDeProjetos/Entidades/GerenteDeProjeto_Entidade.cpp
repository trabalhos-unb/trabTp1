#include "GerenteDeProjeto_Entidade.h"

GerenteDeProjeto::GerenteDeProjeto(){
    this->nome = new Nome();
    this->matricula = new Matricula();
    this->senha = new Senha();
    this->telefone = new Telefone();
};

GerenteDeProjeto::GerenteDeProjeto(Nome *nome, Matricula *matricula, Senha *senha, Telefone *telefone){
    this->nome = nome;
    this->matricula = matricula;
    this->senha = senha;
    this->telefone = telefone;
};

GerenteDeProjeto::~GerenteDeProjeto(){
    delete nome;
    delete matricula;
    delete senha;
    delete telefone;
};
