#include "Projeto.h"

Projeto::Projeto(){
    this->nome = new Nome();
    this->codigo = new CodigoProjeto();
    this->gerente = new GerenteDeProjeto();
    this->dataInicio = new Data();
    this->dataTermino = new Data();
    this->custoAtual = new Custo();
    this->custoPrevisto = new Custo();
    this->estado = new EstadoProjeto();
}

Projeto::Projeto(Nome *nome, CodigoProjeto *codigo, GerenteDeProjeto *gerente, Data *dataInicio, Data *dataTermino, Custo *custoAtual, Custo *custoPrevisto, EstadoProjeto *estado){
    this->nome = nome;
    this->codigo = codigo;
    this->gerente = gerente;
    this->dataInicio = dataInicio;
    this->dataTermino = dataTermino;
    this->custoAtual = custoAtual;
    this->custoPrevisto = custoPrevisto;
    this->estado = estado;
}

Projeto::~Projeto(){
    delete nome;
    delete codigo;
    delete gerente;
    delete dataInicio;
    delete dataTermino;
    delete custoAtual;
    delete custoPrevisto;
    delete estado;
}


void Projeto::addDesenvolvedor(Desenvolvedor *desenvolvedor){
    desenvolvedores.push_back(*desenvolvedor);
}

void Projeto::removerDesenvolvedores(){
    desenvolvedores.clear();
}

int Projeto::quantDesenvolvedores(){
    return desenvolvedores.size();
}
