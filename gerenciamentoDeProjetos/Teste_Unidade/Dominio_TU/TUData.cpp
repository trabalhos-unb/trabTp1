#include "TUData.h"

const int TUData::SUCESSO = 1;
const int TUData::FALHA = 0;

void TUData::setUp(){
    dataProjeto = new Data();
    estadoTeste = SUCESSO;
}

void TUData::tearDown(){
    delete dataProjeto;
}

void TUData::sucessoFormatoData(){
    try{
        dataProjeto->setData("31/12/2050");
    }
    catch(invalid_argument excecao){
        cout << excecao.what() << endl;
        estadoTeste = FALHA;
    }
}

void TUData::falhaFormatoData(){
    try{
        dataProjeto->setData("1/12/2016");
        estadoTeste = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

void TUData::sucessoDiaCorreto(){
    try{
        dataProjeto->setData("12/12/2017");
    }
    catch(invalid_argument excecao){
        cout << excecao.what() << endl;
        estadoTeste = FALHA;
    }
}

void TUData::falhaDiaErrado(){
    try{
        dataProjeto->setData("42/12/2016");
        estadoTeste = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

void TUData::sucessoMesCorreto(){
    try{
        dataProjeto->setData("01/01/2050");
    }
    catch(invalid_argument excecao){
        cout << excecao.what() << endl;
        estadoTeste = FALHA;
    }
}

void TUData::falhaMesErrado(){
    try{
        dataProjeto->setData("01/13/2050");
        estadoTeste = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

void TUData::sucessoAnoCorreto(){
    try{
        dataProjeto->setData("01/12/2020");
    }
    catch(invalid_argument excecao){
        estadoTeste = FALHA;
        cout << excecao.what() << endl;
    }
}

void TUData::falhaAnoErrado(){
    try{
        dataProjeto->setData("01/01/2015");
        estadoTeste = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUData::run(){
    setUp();
    sucessoFormatoData();
    falhaFormatoData();
    sucessoDiaCorreto();
    falhaDiaErrado();
    sucessoMesCorreto();
    falhaMesErrado();
    sucessoAnoCorreto();
    falhaAnoErrado();
    tearDown();

    return estadoTeste;
}
