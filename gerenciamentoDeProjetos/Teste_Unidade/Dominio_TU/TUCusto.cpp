#include "TUCusto.h"

const int TUCusto::SUCESSO = 0;
const int TUCusto::FALHA = -1;

void TUCusto::setUp(){
    custo = new Custo();
    estadoTeste = SUCESSO;
};

void TUCusto::tearDown(){
    delete custo;
};

void TUCusto::sucessoCustoMaiorQueZero(){
    const static float VALOR_VALIDO = 10.0;
    try{
        custo->setCusto(VALOR_VALIDO);
        if( custo->getCusto() != VALOR_VALIDO){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUCusto::sucessoCustoMaiorQueZero()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUCusto::sucessoCustoIgualZero(){
    const static float VALOR_VALIDO = 0.0;
    try{
        custo->setCusto(VALOR_VALIDO);
        if( custo->getCusto() != VALOR_VALIDO){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUCusto::sucessoCustoIgualZero()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};


void TUCusto::falhaCustoMenorQueZero(){
    const static float VALOR_INVALIDO = -5.0;
    try{
        custo->setCusto(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "TUCusto::" << "falhaCustoMenorQueZero()" << endl;
    }catch(invalid_argument excecao){
        //cout << "TUCusto::" << "falhaCustoMenorQueZero()" << endl;
        //cout << "\t" << excecao.what() << endl;
        return;
    }
};

int TUCusto::run(){
    setUp();

    //sucessoCustoIgualZero();
    sucessoCustoMaiorQueZero();
    falhaCustoMenorQueZero();

    tearDown();
    return estadoTeste;
};
