#include "TUMatricula.h"


const int TUMatricula::SUCESSO = 0;
const int TUMatricula::FALHA = -1;

void TUMatricula::setUp(){
    matricula = new Matricula;
    estadoTeste = SUCESSO;
};

void TUMatricula::tearDown(){
    delete matricula;
};

void TUMatricula::sucessoMatriculaComCincoDigitos(){
    const static string VALOR_VALIDO = "48264";
    try{
        matricula->setMatricula(VALOR_VALIDO);
        if( matricula->getMatricula() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUMatricula::sucessoMatriculaComCincoDigitos()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUMatricula::sucessoMatriculaComDigitos(){
    const static string VALOR_VALIDO = "48264";
    try{
        matricula->setMatricula(VALOR_VALIDO);
        if( matricula->getMatricula() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUTelefone::sucessoMatriculaComDigitos()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUMatricula::falhaMatriculaComCaracterNaoDigito(){
    const static string VALOR_INVALIDO = "4@B69";
    try{
        matricula->setMatricula(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUMatricula::falhaMatriculaComCaracterNaoDigito()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUMatricula::falhaMatriculaComCaracterNaoDigito()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUMatricula::falhaMatriculaSemDigitos(){
    const static string VALOR_INVALIDO = "";
    try{
        matricula->setMatricula(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUMatricula::falhaMatriculaSemDigitos()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUMatricula::falhaMatriculaSemDigitos()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUMatricula::falhaMatriculaComExcessoDigitos(){
    const static string VALOR_INVALIDO = "486532";
    try{
        matricula->setMatricula(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUMatricula::falhaMatriculaComExcessoDigitos()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUMatricula::falhaMatriculaComExcessoDigitos()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

int TUMatricula::run(){
    setUp();

    sucessoMatriculaComCincoDigitos();
    //sucessoMatriculaComDigitos();
    //falhaMatriculaComExcessoDigitos();
    //falhaMatriculaSemDigitos();
    falhaMatriculaComCaracterNaoDigito();

    tearDown();
    return estadoTeste;
}



