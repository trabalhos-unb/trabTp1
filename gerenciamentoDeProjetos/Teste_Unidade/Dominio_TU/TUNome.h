#ifndef TUNOME_H_INCLUDED
#define TUNOME_H_INCLUDED

#include "../../Dominios/Nome_Dominio.h"

/**
\brief Classe que modela os testes para dominio Nome
\ingroup TestesDeUnidade
*/

class TUNome{
    private:
        Nome *nome;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoNomeLetrasMaiusculas();
        void sucessoNomeComCaracterEspaco();
        void falhaNomeSemCaracter();
        void falhaNomeComCaracterMinuscula();
        void falhaNomeComExcessoDeCaracter();
        void falhaNomeComCaracterNaoAlfabetico();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Nome e retorna o estado do teste
        int run();
};

#endif // TUNOME_H_INCLUDED
