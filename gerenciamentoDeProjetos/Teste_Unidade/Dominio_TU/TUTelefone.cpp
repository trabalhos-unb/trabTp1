#include "TUTelefone.h"


const int TUTelefone::SUCESSO = 0;
const int TUTelefone::FALHA = -1;

void TUTelefone::setUp(){
    telefone = new Telefone;
    estadoTeste = SUCESSO;
};

void TUTelefone::tearDown(){
    delete telefone;
};

void TUTelefone::sucessoTelefoneComOitoDigitos(){
    const static string VALOR_VALIDO = "34835332";
    try{
        telefone->setTelefone(VALOR_VALIDO);
        if( telefone->getTelefone() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUTelefone::sucessoTelefoneComOitoDigitos()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUTelefone::sucessoTelefoneComDigitos(){
    const static string VALOR_VALIDO = "34835332";
    try{
        telefone->setTelefone(VALOR_VALIDO);
        if( telefone->getTelefone() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUTelefone::sucessoTelefoneComDigitos()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUTelefone::falhaTelefoneComCaracterNaoDigito(){
    const static string VALOR_INVALIDO = "-48E5EE2";
    try{
        telefone->setTelefone(VALOR_INVALIDO);
        estadoTeste = FALHA;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUTelefone::falhaTelefoneComCaracterNaoDigito()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUTelefone::falhaTelefoneSemDigitos(){
    const static string VALOR_INVALIDO = "";
    try{
        telefone->setTelefone(VALOR_INVALIDO);
        estadoTeste = FALHA;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUTelefone::falhaTelefoneSemDigitos()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUTelefone::falhaTelefoneComExcessoDigitos(){
    const static string VALOR_INVALIDO = "934835332";
    try{
        telefone->setTelefone(VALOR_INVALIDO);
        estadoTeste = FALHA;
    }catch(invalid_argument excecao){
                //cout << "Exceção em " << "TUTelefone::falhaTelefoneComExcessoDigitos()" << endl;
        //cout << " por " << excecao.what() << endl;
    }
};

int TUTelefone::run(){
    setUp();

    sucessoTelefoneComOitoDigitos();
    sucessoTelefoneComDigitos();
    falhaTelefoneComExcessoDigitos();
    falhaTelefoneSemDigitos();
    falhaTelefoneComCaracterNaoDigito();

    tearDown();
    return estadoTeste;
}
