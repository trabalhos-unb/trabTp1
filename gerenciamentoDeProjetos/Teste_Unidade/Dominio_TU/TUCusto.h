#ifndef TUCUSTO_H
#define TUCUSTO_H

#include "../../Dominios/Custo_Dominio.h"

/**
\brief Classe que modela os testes para dominio Custo
\ingroup TestesDeUnidade
*/

class TUCusto{
    private:
        Custo *custo;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoCustoIgualZero();
        void sucessoCustoMaiorQueZero();
        void falhaCustoMenorQueZero();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Custo e retorna o estado do teste
        int run();
};

#endif // TUCUSTO_H
