#include "TUEstadoProjeto.h"

const int TUEstadoProjeto::VALOR_VALIDO = 1;
const int TUEstadoProjeto::VALOR_INVALIDO = 3;
const int TUEstadoProjeto::SUCESSO = 1;
const int TUEstadoProjeto::FALHA = 0;

void TUEstadoProjeto::setUp(){
    estadoProjeto = new EstadoProjeto();
    estado = SUCESSO;
}

void TUEstadoProjeto::tearDown(){
    delete estadoProjeto;
}

void TUEstadoProjeto::sucesso(){
    try{
        estadoProjeto->setEstadoProjeto(VALOR_VALIDO);
        if(estadoProjeto->getEstadoProjeto() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUEstadoProjeto::falha(){
    try{
        estadoProjeto->setEstadoProjeto(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }

}

int TUEstadoProjeto::run(){
    setUp();
    sucesso();
    falha();
    tearDown();
    return estado;
}
