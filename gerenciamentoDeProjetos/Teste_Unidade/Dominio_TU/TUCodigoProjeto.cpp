#include "TUCodigoProjeto.h"

const int TUCodigoProjeto::SUCESSO = 1;
const int TUCodigoProjeto::FALHA = 0;
const string TUCodigoProjeto::STRING_VALIDA = "ABCDE";
const string TUCodigoProjeto::STRING_INVALIDA = "ABCD";

void TUCodigoProjeto::setUp(){
    codigo = new CodigoProjeto();
    estado = SUCESSO;
}

void TUCodigoProjeto::tearDown(){
    delete codigo;
}

void TUCodigoProjeto::sucessoQuantValida(){
    try{
        codigo->setCodigoProjeto(STRING_VALIDA);
        if(codigo->getCodigoProjeto() != STRING_VALIDA)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
        cout << excecao.what() << endl;
    }
}

void TUCodigoProjeto::falhaQuantInvalida(){
    try{
        codigo->setCodigoProjeto(STRING_INVALIDA);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

void TUCodigoProjeto::sucessoLetrasMaiusculas(){
     try{
        codigo->setCodigoProjeto(STRING_VALIDA);
        if(codigo->getCodigoProjeto() != STRING_VALIDA)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
        cout << excecao.what() << endl;
    }
}

void TUCodigoProjeto::falhaLetrasMinusculas(){
    try{
        codigo->setCodigoProjeto("abcde");
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

void TUCodigoProjeto::sucessosLetras(){
      try{
        codigo->setCodigoProjeto(STRING_VALIDA);
        if(codigo->getCodigoProjeto() != STRING_VALIDA)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        cout << excecao.what() << endl;
        estado = FALHA;
    }
}

void TUCodigoProjeto::falhaLetras(){
    try{
        codigo->setCodigoProjeto("ABC12");
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}
int TUCodigoProjeto::run(){
    setUp();
    sucessoQuantValida();
    falhaQuantInvalida();
    sucessoLetrasMaiusculas();
    falhaLetrasMinusculas();
    sucessosLetras();
    falhaLetras();
    tearDown();
    return estado;
}
