#ifndef TUTELEFONE_H
#define TUTELEFONE_H

#include "../../Dominios/Telefone_Dominio.h"

/**
\brief Classe que modela os testes para dominio Telefone
\ingroup TestesDeUnidade
*/

class TUTelefone{
    private:
        Telefone *telefone;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoTelefoneComOitoDigitos();
        void sucessoTelefoneComDigitos();
        void falhaTelefoneComExcessoDigitos();
        void falhaTelefoneSemDigitos();
        void falhaTelefoneComCaracterNaoDigito();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Telefone e retorna o estado do teste
        int run();
};

#endif // TUTELEFONE_H
