#ifndef TUDATA_H
#define TUDATA_H

#include "Data.h"

/**
\brief Classe que modela os testes para dominio Data
\ingroup TestesDeUnidade
*/

class TUData{
    private:
        Data *dataProjeto;

        void setUp();
        void tearDown();
        void sucessoFormatoData();
        void falhaFormatoData();
        void sucessoDiaCorreto();
        void falhaDiaErrado();
        void sucessoMesCorreto();
        void falhaMesErrado();
        void sucessoAnoCorreto();
        void falhaAnoErrado();

        int estadoTeste;
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Data e retorna o estado do teste
        int run();

};

#endif // TUDATA_H
