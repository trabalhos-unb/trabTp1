#include "TUFuncao.h"
const int TUFuncao::SUCESSO = 1;
const int TUFuncao::FALHA = 0;

void TUFuncao::setUp(){
    funcaoProjeto = new Funcao();
    estado = SUCESSO;
}

void TUFuncao::tearDown(){
    delete funcaoProjeto;
}

void TUFuncao::falhaFuncaoInvalida(){
    try{
        funcaoProjeto->setFuncao(4);
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUFuncao::sucessoFuncaoValida(){
    try{
        funcaoProjeto->setFuncao(1);
        estado = SUCESSO;
    }
    catch(invalid_argument excecao){
        cout << excecao.what() << endl;
        estado = FALHA;
    }
}

int TUFuncao::run(){

    setUp();
    falhaFuncaoInvalida();
    sucessoFuncaoValida();
    tearDown();
    return estado;
}
