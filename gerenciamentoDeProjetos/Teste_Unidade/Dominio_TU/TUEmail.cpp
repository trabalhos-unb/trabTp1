#include "TUEmail.h"

const int TUEmail::SUCESSO = 0;
const int TUEmail::FALHA = -1;

void TUEmail::setUp(){
    email = new Email();
    estadoTeste = SUCESSO;
};

void TUEmail::tearDown(){
    delete email;
};

void TUEmail::sucesso(){
    const static string EMAIL_VALIDO = "danilosantos@gmail.com";
    try{
        email->setEmail(EMAIL_VALIDO);
        if( email->getEmail() != EMAIL_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUEmail::sucesso()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUEmail::falha(){
    const static string EMAIL_INVALIDO = "";
    try{
        email->setEmail(EMAIL_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUEmail::falha()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUEmail::sucesso()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

int TUEmail::run(){
    setUp();

    sucesso();
    falha();

    tearDown();
    return estadoTeste;
};
