#include "TUFaseProjeto.h"

const int TUFaseProjeto::VALOR_VALIDO = 4;
const int TUFaseProjeto::VALOR_INVALIDO = 10;
const int TUFaseProjeto::FALHA = 0;
const int TUFaseProjeto::SUCESSO = 1;

void TUFaseProjeto::setUp(){
    faseProjeto = new FaseProjeto();
    estado = SUCESSO;
}

void TUFaseProjeto::tearDown(){
    delete faseProjeto;
}

void TUFaseProjeto::sucesso(){
    try{
        faseProjeto->setFaseProjeto(VALOR_VALIDO);
        if(faseProjeto->getFaseProjeto() != VALOR_VALIDO)
            estado = FALHA;
    }
    catch(invalid_argument excecao){
        estado = FALHA;
    }
}

void TUFaseProjeto::falha(){
    try{
        faseProjeto->setFaseProjeto(VALOR_INVALIDO);
        estado = FALHA;
    }
    catch(invalid_argument excecao){
        return;
    }
}

int TUFaseProjeto::run(){
    setUp();
    sucesso();
    falha();
    tearDown();
    return estado;
}
