#ifndef TUESTADOPROJETO_H
#define TUESTADOPROJETO_H

#include "Estado_Projeto.h"

/**
\brief Classe que modela os testes para dominio EstadoProjeto
\ingroup TestesDeUnidade
*/

class TUEstadoProjeto
{
    private:
        const static int VALOR_VALIDO;
        const static int VALOR_INVALIDO;
        int estado;
        EstadoProjeto *estadoProjeto;
        void setUp();
        void tearDown();
        void sucesso();
        void falha();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int FALHA;
        ///Constante utilizada para verificar falha no teste
        const static int SUCESSO;
        ///Executa casos de teste definidos para o dominio EstadoProjeto e retorna o estado do teste
        int run();
};

#endif // TUESTADOPROJETO_H
