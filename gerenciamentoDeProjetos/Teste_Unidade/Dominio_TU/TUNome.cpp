#include "TUNome.h"

const int TUNome::SUCESSO = 0;
const int TUNome::FALHA = -1;

void TUNome::setUp(){
    nome = new Nome;
    estadoTeste = SUCESSO;
}

void TUNome::tearDown(){
    delete nome;
}

void TUNome::sucessoNomeLetrasMaiusculas(){
    const static string VALOR_VALIDO = "DANILO";
    try{
        nome->setNome(VALOR_VALIDO);
        if( nome->getNome() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUNome::sucessoNomeLetrasMaiusculas(()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
}

void TUNome::sucessoNomeComCaracterEspaco(){
    const static string VALOR_VALIDO = "DANILO SANTOS";
    try{
        nome->setNome(VALOR_VALIDO);
        if( nome->getNome() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUNome::sucessoNomeComCaracterEspaco()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
}

void TUNome::falhaNomeSemCaracter(){
    const static string VALOR_INVALIDO = "";
    try{
        nome->setNome(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUNome::falhaNomeSemCaracter()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::falhaNomeSemCaracter()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
}

void TUNome::falhaNomeComCaracterMinuscula(){
    const static string VALOR_INVALIDO = "DANiLo";
    try{
        nome->setNome(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUNome::falhaNomeComCaracterMinuscula()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::falhaNomeComCaracterMinuscula()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
}

void TUNome::falhaNomeComExcessoDeCaracter(){
    const static string VALOR_INVALIDO = "DANILODANILODANILODANILO";
    try{
        nome->setNome(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUNome::falhaNomeComExcessoDeCaracter()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::falhaNomeComExcessoDeCaracter()" << endl;
        //cout << " por " << excecao.what() << endl;
    }
}

void TUNome::falhaNomeComCaracterNaoAlfabetico(){
    const static string VALOR_INVALIDO = "D@N1LO";
    try{
        nome->setNome(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUNome::falhaNomeComCaracterNaoAlfabetico()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::falhaNomeComCaracterNaoAlfabetico()" << endl;
        //cout << " por " << excecao.what() << endl;
    }
}

int TUNome::run(){
    setUp();

    sucessoNomeLetrasMaiusculas();
    //sucessoNomeComCaracterEspaco();
    falhaNomeSemCaracter();
    //falhaNomeComCaracterMinuscula();
    //falhaNomeComExcessoDeCaracter();
    //falhaNomeComCaracterNaoAlfabetico();

    tearDown();
    return estadoTeste;
}

