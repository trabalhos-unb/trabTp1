#ifndef TUFUNCAO_H
#define TUFUNCAO_H
#include "Funcao.h"

/**
\brief Classe que modela os testes para dominio Funcao
\ingroup TestesDeUnidade
*/

class TUFuncao
{
    private:
        Funcao *funcaoProjeto;
        int estado;
        void setUp();
        void tearDown();
        void sucessoFuncaoValida();
        void falhaFuncaoInvalida();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Funcao e retorna o estado do teste
        int run();
};

#endif // TUFUNCAO_H
