#ifndef TUMATRICULA_H
#define TUMATRICULA_H

#include "../../Dominios/Matricula_Dominio.h"

/**
\brief Classe que modela os testes para dominio Matricula
\ingroup TestesDeUnidade
*/

class TUMatricula{
    private:
        Matricula *matricula;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoMatriculaComCincoDigitos();
        void sucessoMatriculaComDigitos();
        void falhaMatriculaComExcessoDigitos();
        void falhaMatriculaSemDigitos();
        void falhaMatriculaComCaracterNaoDigito();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Matricula e retorna o estado do teste
        int run();
};

#endif // TUMATRICULA_H
