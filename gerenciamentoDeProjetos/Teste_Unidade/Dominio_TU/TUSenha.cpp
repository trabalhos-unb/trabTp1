#include "TUSenha.h"

const int TUSenha::SUCESSO = 0;
const int TUSenha::FALHA = -1;

void TUSenha::setUp(){
    senha = new Senha;
    estadoTeste = SUCESSO;
};

void TUSenha::tearDown(){
    delete senha;
};

void TUSenha::sucessoSenhaComCincoCaracter(){
    const static string VALOR_VALIDO = "45871";
    try{
        senha->setSenha(VALOR_VALIDO);
        if( senha->getSenha() != VALOR_VALIDO ){
            estadoTeste = FALHA;
        }
    }catch(invalid_argument excecao){
        cout << "Exceção em " << "TUSenha::sucessoSenhaComCincoCaracter()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

void TUSenha::falhaSenhaSemCaracter(){
    const static string VALOR_INVALIDO = "";
    try{
        senha->setSenha(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUSenha::falhaSenhaSemCaracter()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em "<< "TUSenha::falhaSenhaSemCaracter()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUSenha::falhaSenhaComExcessoCaracter(){
    const static string VALOR_INVALIDO = "1234567";
    try{
        senha->setSenha(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUSenha::falhaSenhaComExcessoCaracter()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::" << "TUSenha::falhaSenhaComExcessoCaracter()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

void TUSenha::falhaSenhaComCaracterRepetido(){
    const static string VALOR_INVALIDO = "48564";
    try{
        senha->setSenha(VALOR_INVALIDO);
        estadoTeste = FALHA;
        cout << "Exceção em " << "TUSenha::falhaSenhaComCaracterRepetido()" << endl;
    }catch(invalid_argument excecao){
        //cout << "Exceção em " << "TUNome::" << "TUSenha::falhaSenhaComCaracterRepetido()" << endl;
        //cout << " por " << excecao.what() << endl;
        return;
    }
};

int TUSenha::run(){
    setUp();

    sucessoSenhaComCincoCaracter();
    //falhaSenhaComExcessoCaracter();
   // falhaSenhaSemCaracter();
    falhaSenhaComCaracterRepetido();

    tearDown();
    return estadoTeste;
}

