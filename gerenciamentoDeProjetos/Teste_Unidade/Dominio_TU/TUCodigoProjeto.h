#ifndef TUCODIGOPROJETO_H
#define TUCODIGOPROJETO_H

#include "Codigo_Projeto.h"

/**
\brief Classe que modela os testes para dominio CodigoProjeto
\ingroup TestesDeUnidade
*/

class TUCodigoProjeto
{
    private:
        CodigoProjeto *codigo;
        int estado;

        void setUp();
        void tearDown();
        void sucessoQuantValida();
        void falhaQuantInvalida();
        void sucessoLetrasMaiusculas();
        void falhaLetrasMinusculas();
        void sucessosLetras();
        void falhaLetras();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
         ///Executa casos de teste definidos para o dominio CodigoProjeto e retorna o estado do teste
        int run();

        const static string STRING_VALIDA;
        const static string STRING_INVALIDA;
};

#endif // TUCODIGOPROJETO_H
