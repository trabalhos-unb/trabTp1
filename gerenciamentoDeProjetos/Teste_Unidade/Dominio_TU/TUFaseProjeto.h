#ifndef TUFASEPROJETO_H
#define TUFASEPROJETO_H
#include "Fase_Projeto.h"

/**
\brief Classe que modela os testes para dominio FaseProjeto
\ingroup TestesDeUnidade
*/

class TUFaseProjeto
{

    private:
        const static int VALOR_VALIDO;
        const static int VALOR_INVALIDO;

        FaseProjeto *faseProjeto;

        void setUp();
        void tearDown();
        void sucesso();
        void falha();
        int estado;
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
       ///Executa casos de teste definidos para o dominio FaseProjeto e retorna o estado do teste
        int run();

};

#endif // TUFASEPROJETO_H
