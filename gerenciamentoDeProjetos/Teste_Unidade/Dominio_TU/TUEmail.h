#ifndef TUEMAIL_H
#define TUEMAIL_H

#include "../../Dominios/Email_Dominio.h"

/**
\brief Classe que modela os testes para dominio Email
\ingroup TestesDeUnidade
*/

class TUEmail{
    private:
        Email *email;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucesso();
        void falha();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Email e retorna o estado do teste
        int run();
};

#endif // TUEMAIL_H
