#ifndef TUSENHA_H
#define TUSENHA_H

#include "../../Dominios/Senha_Dominio.h"

/**
\brief Classe que modela os testes para dominio Senha
\ingroup TestesDeUnidade
*/

class TUSenha{
    private:
        Senha *senha;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoSenhaComCincoCaracter();
        void falhaSenhaComExcessoCaracter();
        void falhaSenhaSemCaracter();
        void falhaSenhaComCaracterRepetido();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para o dominio Senha e retorna o estado do teste
        int run();
};

#endif // TUSENHA_H
