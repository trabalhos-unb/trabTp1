#include "TUGerenteDeSistema.h"

const int TUGerenteDeSistema::SUCESSO = 0;
const int TUGerenteDeSistema::FALHA = -1;

void TUGerenteDeSistema::setUp(){
    //gerenteDeSistema = new GerenteDeSistema();
    estadoTeste = SUCESSO;
};

void TUGerenteDeSistema::tearDown(){
    delete gerenteDeSistema;
};

void TUGerenteDeSistema::sucessoCriaGerenteDeSistemaArgumentoValido(){
    const static string NOME_VALIDO = "DANILO";
    const static string MATRICULA_VALIDA = "12345";
    const static string SENHA_VALIDA = "12345";

    Nome *nome = new Nome(NOME_VALIDO);
    Matricula *matricula = new Matricula(MATRICULA_VALIDA);
    Senha *senha = new Senha(SENHA_VALIDA);

    try{
        gerenteDeSistema = new GerenteDeSistema(nome, matricula, senha);
    }catch(exception excecao){
        cout << "Exceção em " << "TUGerenteDeSistema::sucessoCriaGerenteDeSistemaArgumentoValido()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

int TUGerenteDeSistema::run(){
    setUp();

    sucessoCriaGerenteDeSistemaArgumentoValido();

    tearDown();
    return estadoTeste;
};
