#ifndef TUFASE_ENTIDADE_H
#define TUFASE_ENTIDADE_H
#include "Fase_Entidade.h"

class TUFaseEntidade{
    private:
        FaseEntidade *fase;
        void setUp();
        void tearDown();
        void sucessoCriaFaseValida();
        void falhaCriaFaseInvalida();
        int estado;

    public:
        const static int SUCESSO;
        const static int FALHA;
        int run();

};

#endif // TUFASE_ENTIDADE_H
