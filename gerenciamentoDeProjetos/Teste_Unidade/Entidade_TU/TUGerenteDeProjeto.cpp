#include "TUGerenteDeProjeto.h"

const int TUGerenteDeProjeto::SUCESSO = 0;
const int TUGerenteDeProjeto::FALHA = -1;

void TUGerenteDeProjeto::setUp(){
    //gerenteDeProjeto = new GerenteDeProjeto();
    estadoTeste = SUCESSO;
};

void TUGerenteDeProjeto::tearDown(){
    delete gerenteDeProjeto;
};

void TUGerenteDeProjeto::sucessoCriaGerenteDeProjetoArgumentoValido(){
    const static string NOME_VALIDO = "DANILO";
    const static string MATRICULA_VALIDA = "12345";
    const static string SENHA_VALIDA = "12345";
    const static string TELEFONE_VALIDO = "34835332";

    Nome *nome = new Nome(NOME_VALIDO);
    Matricula *matricula = new Matricula(MATRICULA_VALIDA);
    Senha *senha = new Senha(SENHA_VALIDA);
    Telefone *telefone = new Telefone(TELEFONE_VALIDO);

    try{
        gerenteDeProjeto = new GerenteDeProjeto(nome, matricula, senha, telefone);
    }catch(exception excecao){
        cout << "Exceção em " << "UGerenteDeProjeto::sucessoCriaGerenteDeProjetoArgumentoValido()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};


int TUGerenteDeProjeto::run(){
    setUp();

    sucessoCriaGerenteDeProjetoArgumentoValido();

    tearDown();
    return estadoTeste;
};
