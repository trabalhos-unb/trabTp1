#ifndef TUGERENTEDEPROJETO_H
#define TUGERENTEDEPROJETO_H

#include "../../Entidades/GerenteDeProjeto_Entidade.h"

/**
\brief Classe que modela os testes para entidade Gerente de Projeto
\ingroup TestesDeUnidade
*/

class TUGerenteDeProjeto{
    private:
        GerenteDeProjeto *gerenteDeProjeto;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoCriaGerenteDeProjetoArgumentoValido();

    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para a entidade GerenteDeProjeto e retorna o estado do teste
        int run();
};

#endif // TUGERENTEDEPROJETO_H
