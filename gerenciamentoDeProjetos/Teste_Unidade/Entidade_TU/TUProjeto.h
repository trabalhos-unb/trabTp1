#ifndef TUPROJETO_H
#define TUPROJETO_H

#include "Projeto.h"

/**
\brief Classe que modela os testes para entidade Projeto
\ingroup TestesDeUnidade
*/

class TUProjeto{
    private:
        Projeto *projeto;
        void setUp();
        void tearDown();
        void cenarioSucesso();
        void cenarioFalha();
        int estado;
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para entidade Projeto e retorna o estado do teste
        int run();
};

#endif // TUPROJETO_H
