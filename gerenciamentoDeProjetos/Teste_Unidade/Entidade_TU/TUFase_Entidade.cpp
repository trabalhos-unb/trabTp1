#include "TUFase_Entidade.h"

const int TUFaseEntidade::SUCESSO = 1;
const int TUFaseEntidade::FALHA = 0;

void TUFaseEntidade::setUp(){
    estado = SUCESSO;
}

void TUFaseEntidade::tearDown(){
    delete fase;
}

void TUFaseEntidade::sucessoCriaFaseValida(){
    const static string DATA_VALIDA_INICIO = "02/08/2016";
    const static string DATA_VALIDA_TERMINO = "02/12/2016";
    const static int INICIACAO = 1;
    try{

        fase = new FaseEntidade(new Data(DATA_VALIDA_INICIO), new Data(DATA_VALIDA_TERMINO),new FaseProjeto(INICIACAO));
    }
    catch(invalid_argument excecao){
        estado = FALHA;
        cout <<excecao.what() << endl;
    }

}

void TUFaseEntidade::falhaCriaFaseInvalida(){
    const static string DATA_INVALIDA_INICIO = "02/08/2015";
    const static string DATA_INVALIDA_TERMINO = "02/12/2060";
    const static int FASE_INVALIDA = 5;

    try{
        fase = new FaseEntidade(new Data(DATA_INVALIDA_INICIO), new Data(DATA_INVALIDA_TERMINO),new FaseProjeto(FASE_INVALIDA));
    }
    catch(invalid_argument excecao){
       return;
    }
}

int TUFaseEntidade::run(){
    setUp();
    sucessoCriaFaseValida();
    falhaCriaFaseInvalida();
    tearDown();

    return estado;
}
