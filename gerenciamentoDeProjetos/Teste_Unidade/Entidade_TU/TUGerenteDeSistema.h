#ifndef TUGERENTEDESISTEMA_H
#define TUGERENTEDESISTEMA_H

#include "../../Entidades/GerenteDeSistema_Entidade.h"

/**
\brief Classe que modela os testes para entidade Gerente de Sistema
\ingroup TestesDeUnidade
*/

class TUGerenteDeSistema{
    private:
        GerenteDeSistema *gerenteDeSistema;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoCriaGerenteDeSistemaArgumentoValido();
        void falhaCriaGerenteDeSistemaArgumentoInvalido();
    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para entidade GerenteDeSistema e retorna o estado do teste
        int run();
};

#endif // TUGERENTEDESISTEMA_H
