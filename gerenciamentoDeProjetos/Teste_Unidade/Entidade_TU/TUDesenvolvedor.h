#ifndef TUDESENVOLVEDOR_H
#define TUDESENVOLVEDOR_H

#include "../../Entidades/Desenvolvedor_Entidade.h"
/**
 * \defgroup TestesDeUnidade
 * @{
 */
/**@}*/

/**
\brief Classe que modela os testes para entidade Desenvolvedor
\ingroup TestesDeUnidade
*/

class TUDesenvolvedor{
    private:
        Desenvolvedor *desenvolvedor;
        int estadoTeste;

        void setUp();
        void tearDown();

        void sucessoCriaDesenvolvedorArgumentoValido();

    public:
        ///Constante utilizada para verificar sucesso no teste
        const static int SUCESSO;
        ///Constante utilizada para verificar falha no teste
        const static int FALHA;
        ///Executa casos de teste definidos para entidade Desenvolvedor e retorna o estado do teste
        int run();
};

#endif // TUDESENVOLVEDOR_H
