#include "TUDesenvolvedor.h"

const int TUDesenvolvedor::SUCESSO = 0;
const int TUDesenvolvedor::FALHA = -1;

void TUDesenvolvedor::setUp(){
    //desenvolvedor = new Desenvolvedor;
    estadoTeste = SUCESSO;
};

void TUDesenvolvedor::tearDown(){
    delete desenvolvedor;
};

void TUDesenvolvedor::sucessoCriaDesenvolvedorArgumentoValido(){
    const static string NOME_VALIDO = "DANILO";
    const static string MATRICULA_VALIDA = "12345";
    const static string SENHA_VALIDA = "12345";
    const static string EMAIL_VALIDO = "mad.dan@live.com";
    const static int FUNCAO_VALIDA = Funcao::ANALISTA;

    Nome *nome = new Nome(NOME_VALIDO);
    Matricula *matricula = new Matricula(MATRICULA_VALIDA);
    Senha *senha = new Senha(SENHA_VALIDA);
    Email *email = new Email(EMAIL_VALIDO);
    Funcao *funcao = new Funcao(FUNCAO_VALIDA);

    try{
        desenvolvedor = new Desenvolvedor(nome, matricula, senha, email, funcao);
    }catch(exception excecao){
        cout << "Exceção em " << "TUDesenvolvedor::sucessoCriaDesenvolvedorArgumentoValido()" << endl;
        cout << " por " << excecao.what() << endl;
        estadoTeste = FALHA;
    }
};

int TUDesenvolvedor::run(){
    setUp();

    sucessoCriaDesenvolvedorArgumentoValido();

    tearDown();
    return estadoTeste;
};
