#include <iostream>

/*Teste de Unidade para Dominios*/
#include "Teste_Unidade/Dominio_TU/TUCusto.h"
#include "Teste_Unidade/Dominio_TU/TUMatricula.h"
#include "Teste_Unidade/Dominio_TU/TUNome.h"
#include "Teste_Unidade/Dominio_TU/TUSenha.h"
#include "Teste_Unidade/Dominio_TU/TUEmail.h"
#include "Teste_Unidade/Dominio_TU/TUTelefone.h"
#include "TUCodigoProjeto.h"
#include "TUEstadoProjeto.h"
#include "TUFaseProjeto.h"
#include "TUFuncao.h"
#include "TUData.h"

/*Teste de Unidade para Entidades*/
#include "Teste_Unidade/Entidade_TU/TUDesenvolvedor.h"
#include "Teste_Unidade/Entidade_TU/TUGerenteDeSistema.h"
#include "Teste_Unidade/Entidade_TU/TUGerenteDeProjeto.h"
#include "TUFase_Entidade.h"
#include "TUProjeto.h"

using namespace std;

/** Função principal onde são executados
 * os testes de unidades e avaliados os
 * resultados dos testes.
*/

int main(){
    /*Dominios*/
    TUNome nome;
    TUCusto custo;
    TUMatricula matricula;
    TUTelefone telefone;
    TUSenha senha;
    TUCodigoProjeto codigoProjeto;
    TUEstadoProjeto estadoProjeto;
    TUFaseProjeto faseDominio;
    TUFuncao funcao;
    TUData data;
    TUEmail email;

    /*Entidades*/
    TUDesenvolvedor desenvolvedor;
    TUGerenteDeProjeto gerenteDeProjeto;
    TUGerenteDeSistema gerenteDeSistema;
    TUFaseEntidade fase;
    TUProjeto projeto;

    /*Teste de Dominios*/
    if( nome.run() == TUNome::FALHA){
        cout << "Falha para Teste de Unidade de Nome." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Nome." << endl;
    }

    if( custo.run() == TUCusto::FALHA){
        cout << "Falha para Teste de Unidade de Custo." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Custo." << endl;
    }

    if( telefone.run() == TUTelefone::FALHA){
        cout << "Falha para Teste de Unidade de Telefone." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Telefone." << endl;
    }

    if( senha.run() == TUSenha::FALHA){
        cout << "Falha para Teste de Unidade de Senha." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Senha." << endl;
    }

    if( matricula.run() == TUMatricula::FALHA){
        cout << "Falha para Teste de Unidade de Matricula." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Matricula." << endl;
    }

    if(codigoProjeto.run() == TUCodigoProjeto::FALHA){
        cout << "Falha para o teste de unidade de Codigo de projeto." << endl;
    }
    else{
        cout << "Sucesso para o teste de unidade de Codigo de projeto." << endl;
    }
    if(estadoProjeto.run() == TUEstadoProjeto::FALHA){
        cout << "Falha para Teste de Unidade de Estado de projeto." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Estado de projeto." << endl;
    }

    if(faseDominio.run() == TUFaseProjeto::FALHA){
        cout << "Falha para Teste de Unidade de Fase(Dominio)." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Fase(Dominio)." << endl;
    }

    if(funcao.run() == TUFuncao::FALHA){
        cout << "Falha para Teste de Unidade de Funcao." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Funcao." << endl;
    }
    if( email.run() == TUEmail::FALHA){
        cout << "Falha para Teste de Unidade de Email." << endl << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Email." << endl << endl;
    }

    if(data.run() == TUData::FALHA){
        cout << "Falha para Teste de Unidade de Data." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Data." << endl;
    }

    /*Teste Entidades*/
    if( gerenteDeSistema.run() == TUGerenteDeSistema::FALHA){
        cout << "Falha para Teste de Unidade de GerenteDeSistema." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de GerenteDeSistema." << endl;
    }

    if( gerenteDeProjeto.run() == TUGerenteDeProjeto::FALHA){
        cout << "Falha para Teste de Unidade de GerenteDeProjeto." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de GerenteDeProjeto." << endl;
    }

    if( desenvolvedor.run() == TUDesenvolvedor::FALHA){
        cout << "Falha para Teste de Unidade de Desenvolvedor." << endl;
    }else{
        cout << "Sucesso para o Teste de Unidade de Desenvolvedor." << endl;
    }
    if(fase.run() == TUFaseEntidade::FALHA){
        cout << "Falha para Teste de Unidade da entidade Fase." << endl;
    }
    else{
        cout << "Sucesso para o Teste de Unidade da entidade Fase." << endl;
    }

    if(projeto.run() == TUProjeto::FALHA){
        cout << "Falha para teste de Unidade do Projeto" << endl;
    }else{
        cout << "Sucesso para teste de Unidade do Projeto" << endl;
    }

    return 0;
};
