#ifndef CODIGO_PROJETO_H
#define CODIGO_PROJETO_H

#include <string>
#include <iostream>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio CodigoProjeto
\ingroup Dominios
*/

class CodigoProjeto {
    private:
        string codigo;
        ///valida o codigo de acordo com a string passada (letras maiusculas de A até Z)
        void validaCodigo(string) throw (invalid_argument);
        const static int totalLetras;

    public:
        ///Construtor padrao, instancia o objeto com o valor padrao
        CodigoProjeto();
        ///Construtor que instancia o objeto com a string passada
        CodigoProjeto(string);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~CodigoProjeto();
        string getCodigoProjeto() const;
        void setCodigoProjeto(string);
};

#endif // CODIGO_PROJETO_H
