#ifndef SENHA_DOMINIO_H_INCLUDED
#define SENHA_DOMINIO_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>

using namespace std;

/**
\brief Classe que modela o dominio Senha
\ingroup Dominios
*/

class Senha{
    private:
        const static unsigned int maxCaracter = 5;
        string senha;
        void validaSenha(string) throw(invalid_argument);
    public:
        ///Construtor padrão de Senha, instancia objetos com valor padrão
        Senha();
        /** Construtor de Senha,
        * recebe uma string de cinco caracteres,
        * podendo estes ser alfabeticos, ou digitos.
        */
        Senha(string);
        ///Destrutor padrão do objeto
        ~Senha();
        ///Permite extrair o atual valor de Senha
        string getSenha() const;
        ///Permite atualizar o valor de Senha. Lança exceção em caso de argumento inválido.
        void setSenha(string);
};


#endif // SENHA_DOMINIO_H_INCLUDED
