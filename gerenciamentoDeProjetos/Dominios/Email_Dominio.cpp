#include "Email_Dominio.h"

Email::Email(){
    email = "";
};

Email::Email(string email){
    validaEmail(email);
    this->email = email;
};

Email::~Email(){
};

void Email::validaEmail(string email) throw(invalid_argument){
    if( email.length() == 0 ){
        throw invalid_argument("*Email sem caracter");
    }
};

string Email::getEmail() const{
    return email;
};

void Email::setEmail(string novoEmail){
    validaEmail(novoEmail);
    this->email = novoEmail;
};
