#ifndef MATRICULA_DOMINIO_H_INCLUDED
#define MATRICULA_DOMINIO_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio Matricula
\ingroup Dominios
*/

class Matricula{
    private:
        const static unsigned int maxCaracter = 5;
        string matricula;
        void validaMatricula(string novaMatricula) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia objetos com valor padrão
        Matricula();
        /**Construtor de Matricula,
        * recebe como argumento um string
        * de 5 digitos de 0-9.
        */
        Matricula(string);
        ///Destrutor padrão do objeto
        ~Matricula();
        ///Permite extrair atual valor do objeto Matricula
        string getMatricula() const;
        ///Permite atualizar valor do objeto Matricula. Lança exceção em caso de argumento inválido
        void setMatricula(string);
};

#endif // MATRICULA_DOMINIO_H_INCLUDED
