#ifndef DATA_H
#define DATA_H

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>

/**
 * \defgroup Dominios
 * @{
 */
/**@}*/


/**
\brief Classe que modela o dominio Data
\ingroup Dominios
*/


using namespace std;

/**
\brief Classe que modela o dominio Data
\ingroup Dominios
*/

class Data
{
    private:
        const static int TOTAL_ELEMENTOS_DATA;
        string data;
        int dia, mes, ano;
        void validarData(string) throw (invalid_argument);
        void validarDia(string) throw (invalid_argument);
        void validarMes(string) throw (invalid_argument);
        void validarAno(string) throw (invalid_argument);
    public:
        ///Construtor padrão, instancia os objetos com os valores padrao
        Data();
        ///Construtor para Data passando como argumento uma string com o formato DD/MM/AAAA
        Data(string);
        void setData(string);
        string getData() const;

};

#endif // DATA_H
