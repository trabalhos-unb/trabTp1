#include "Nome_Dominio.h"

Nome::Nome(){
    nome = "";
};

Nome::Nome(string nome){
    validaNome(nome);
    this->nome = nome;
};

Nome::~Nome(){
};

void Nome::validaNome(string novoNome) throw(invalid_argument){
    /*Procura por string maiores que o tamanho maximo*/
    if( novoNome.length() > max_caracter ){
        throw invalid_argument("*Nome com mais de 20 caracteres");
    }
    /*Procura por string em branco*/
    if( !novoNome.compare("") ){
        throw invalid_argument("*Nome sem caracteres");
    }
    /*Procura por letra de A-Z nao maiusculas*/
    for(unsigned int indice = 0 ; indice < novoNome.length() ; indice++){
        if( std::isalpha(novoNome[indice]) ){
            if( !std::isupper(novoNome[indice])  ){
                throw invalid_argument("*Caracateres de A-Z não maiúsculos");
            }
        }
    }
};

string Nome::getNome() const{
    return nome;
};


void Nome::setNome(string novoNome) throw(invalid_argument){
    validaNome(novoNome);
    nome = novoNome;
};
